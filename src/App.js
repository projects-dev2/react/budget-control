import {useState} from "react";

import {Question} from "./components/Question";
import {Form} from "./components/Form";
import {ListExpense} from "./components/ListExpense";
import {BudgetControl} from "./components/BudgetControl";

import './App.css';

function App() {

    console.log = ()=> {}

    const [budget, setBudget] = useState(0);
    const [rest, setRest] = useState(0);
    const [listExpenses, setListExpenses] = useState([]);

    return (
        <div className="container">
            <header>
                <h1>Budget Control</h1>

                <div className="content content-main">

                    {budget === 0 ?

                        (<Question setBudget={setBudget} setRest={setRest}/>) :

                        (
                            <div className="row">

                                <div className="one-half column">
                                    <Form listExpenses={listExpenses}
                                          rest={rest}
                                          setRest={setRest}
                                          setListExpenses={setListExpenses}
                                    />
                                </div>

                                <div className="one-half column">

                                    {listExpenses.length !== 0 && <ListExpense listExpenses={listExpenses}/>}
                                    <BudgetControl budget={budget} rest={rest}/>
                                </div>

                            </div>
                        )
                    }

                </div>

            </header>
        </div>
    );
}

export default App;
