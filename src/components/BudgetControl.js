import React from 'react';
import {ReviewBudget} from "../helpers/ReviewBudget";
import PropTypes from "prop-types";

export const BudgetControl = ({budget, rest}) => {
    return (

        <>
            <div className="alert alert-primary">
                Budget: $/.{budget}
            </div>
            <div className={ReviewBudget(budget, rest)}>
                <strong>Rest: </strong> $/.{rest}
            </div>
        </>

    )
};

BudgetControl.propTypes = {
    budget: PropTypes.number.isRequired,
    rest: PropTypes.number.isRequired,
}