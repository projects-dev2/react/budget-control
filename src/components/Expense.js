import React from 'react';
import PropTypes from "prop-types";

export const Expense = ({expense}) => {
    return (

        <li className="expenses">
            <p>

                {expense.nameExpense}
                <span className="expense">$/.{expense.amountExpense}</span>

            </p>
        </li>

    )
};

Expense.propTypes = {
    expense: PropTypes.object.isRequired,
}