import React, {useState} from 'react';
import {Error} from "./Error";
import uniqid from 'uniqid';
import PropTypes from "prop-types";

export const Form = ({setListExpenses, listExpenses, setRest, rest}) => {

    const [nameExpense, setNameExpense] = useState('');
    const [amountExpense, setAmountExpense] = useState(0);
    const [error, setError] = useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();

        // To validate data
        if (nameExpense.trim() === '' || amountExpense < 1 || isNaN(amountExpense)) {
            setError(true);
            return 1;
        }

        setError(false);

        // to create th object
        const data = {
            nameExpense,
            amountExpense,
            id: uniqid()
        }

        // to add expense
        setListExpenses([
            ...listExpenses,
            data
        ]);

        // Subtract a budget
        let restNew = rest - amountExpense;
        setRest(restNew);

        // reset states
        setAmountExpense(0);
        setNameExpense('');
    }

    return (

        <form onSubmit={handleSubmit}>
            <h2>Add your expense here</h2>

            {error && <Error message="The fields is required"/>}

            <div className="field">

                <label id="nameExpense">Name's Expense: </label>
                <input type="text" id="nameExpense" className="u-full-width"
                       placeholder="Eg Food" value={nameExpense}
                       onChange={e => setNameExpense(e.target.value)}/>

                <label id="amountExpense">Amount's Expense: ($/.) </label>
                <input type="number" id="amountExpense" className="u-full-width"
                       placeholder="Eg 300" value={amountExpense}
                       onChange={e => setAmountExpense(parseInt(e.target.value) || '')}/>

                <input type="submit" className="button-primary u-full-width"
                       value="Add expense"/>

            </div>

        </form>

    )
};

Form.propTypes = {
    listExpenses: PropTypes.array.isRequired,
    rest: PropTypes.number.isRequired,
    setListExpenses: PropTypes.func.isRequired,
    setRest: PropTypes.func.isRequired,
}