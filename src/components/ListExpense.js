import React from 'react';
import {Expense} from "./Expense";
import PropTypes from "prop-types";

export const ListExpense = ({listExpenses}) => {

    console.log(listExpenses);
    return (

        <div className="expenses-realized">

            <h2>List Expenses</h2>

            {listExpenses.length !== 0 &&
                listExpenses.map(expense =>
                    (<Expense key={expense.id} expense={expense}/>)
                )
            }

        </div>

    )
};

ListExpense.propTypes = {
    listExpenses: PropTypes.array.isRequired,
}