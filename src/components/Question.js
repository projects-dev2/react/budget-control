import React, {useState} from 'react';
import {Error} from "./Error";
import PropTypes from 'prop-types';

export const Question = ({setBudget, setRest}) => {

    const [amount, setAmount] = useState(0); // Budget amount
    const [error, setError] = useState(false);

    const handleSubmit = (e) => {

        e.preventDefault();

        // To validate form
        if (amount < 1) { // if not a number
            setError(true);
            return 1;
        }

        setError(false);
        setBudget(amount);
        setRest(amount);

    }

    const handleChange = ({target}) => {
        setAmount(parseInt(target.value, 10));
    }

    return (

        <div>
            <h2>Budget</h2>

            {error && <Error message="The budget is an incorrect"/>}

            <form onSubmit={handleSubmit}>
                <input type="number" name="budget" className="u-full-width"
                       placeholder="Write your budget" onChange={handleChange}/>

                <input type="submit" value="Establish"
                       className="button-primary u-full-width"/>
            </form>
        </div>

    )
};

Question.propTypes = {
    setBudget: PropTypes.func.isRequired,
    setRest: PropTypes.func.isRequired,
}