export const ReviewBudget = (budget, rest) => {

    let nameClase;

    if ((budget / 4) >= rest) { // 75% was exceeded
        nameClase = "alert alert-danger";
    } else if ((budget / 2) >= rest) { // 55% was exceeded
        nameClase = "alert alert-warning";
    } else {
        nameClase = "alert alert-success";
    }

    return nameClase;

}